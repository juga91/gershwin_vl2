\version "2.18.2"

\header {
  title = "Rhapsody in Blue"
  composer = "George Gershwin"
}

global = {
  \time 4/4

}

%To do: Dynamik, Vortragsangaben, Stichnoten

violineTwo = \relative c' {
  \global
  \key bes \major
    \set Score.markFormatter = #format-mark-circle-numbers
  R1*10
  \mark \default
  <g es'>1~
  <g es'>~
  <g es'>~
  <g es'>2.
 \tuplet 3/2 {bes8( b c)}
 des1\fermata(
 \mark \default
 \bar "||" 
 \key aes \major 
 c4) r r2
 R1*3
 r2 r4 \tuplet 3/2 {des'!8\accent es\accent f\accent}
 \tupletSpan 4
 \mark \default
 ges2\accent \tuplet 3/2 {fes8\accent eses fes eses\accent fes eses}
 des8\accent ces bes a\accent~ a ces c des\accent~
 des bes ges fes\accent~ fes eses des4
 \key a \major
 \mark \default
 <bes ges'>8\accent r8 r4 r2
 \compressFullBarRests
 R1*5
\mark "A"
 \bar "||" 
 R1*7 
 R1\fermataMarkup
 R1*3
 \mark \default
 cis4\accent cis\accent cis\accent cis\accent
 cis\accent cis\accent d\accent d\accent
 e\accent r r2
 R1*2 
 cis4\accent cis\accent cis\accent cis\accent
 cis\accent cis\accent cis\accent cis\accent 
 c\accent r r2
 R1*5
 R1\fermataMarkup
 R1*10
 \mark "B"
 R1*7
 \bar "||"
 \mark \default
 <cis a'>4 <cis a'> <b gis'?> <b gis'>
 <cis a'>4 <cis a'> <a fis'> <a fis'>
 <a e'> <a e'> <gis! d'> <gis d'>
 <a e'> r r2 
 r r4 <b gis'>\accent
<cis a'>4 <cis a'> <b gis'?> <b gis'>
 <cis a'>4 <cis a'> <a fis'> <a fis'>
 <a e'> <a e'> <gis! d'> <gis d'>
 <a e'>  <a e'>  <a e'>  <a e'>
 \mark \default
 r8 a\accent( c) d e\accent( f4.)
 r8 a,\accent( c) d f\accent( a4.)
 r8 a,\accent( c) d f g a c\accent~
 c1
 \mark \default
 \bar "||"
 \key c \major 
 r4 <es, c' g'>\accent r4 <d b'? g'>\accent
 r4 <e! c' g'>\accent r <d c' f>\accent
 r <c g' es'>\accent r <c g' es'>\accent
 r <c g' es'>\accent r <c ges' es'>\accent
  <c g'! e'!>\accent r r2
 <b f' d'>4 r r2
 \mark \default
 <c e>4.\accent <c e>8\accent~ <c e>4 <c e>\accent
 <d f>4.\accent <d f>8\accent~ <d f>4 <d f>\accent
 <c e>4.\accent <c e>8\accent~ <c e>4 <c e>\accent
 <c es>4. <c es>8\accent~ <c es>4 <c es>\accent
 <c e!>4.\accent <c e>8\accent~ <c e>4 <c e>\accent
 <d f>4.\accent <d f>8\accent~ <d f>4 <d f>\accent
 <c bes'>4.\accent <c bes'>8\accent~ <c bes'>4 <c bes'>\accent
 <c bes'>4.\accent <c bes'>8\accent~ <c bes'>4 <c bes'>\accent
 \mark \default
 \repeat percent 4 {<c f>4.\accent <c f>8\accent~ <c f>4 <c f>\accent}
 <c e>4.\accent <c e>8\accent~ <c e>4 <c e>\accent
 <d f>4.\accent <d f>8\accent~ <d f>4 <d f>\accent
 \mark \default
 R1 
 r4 c8\accent^\markup{\italic "Soli"} e\accent g\accent c\accent e\accent g\accent
 e4\accent r r2
 R1*2
 r4 a,,8\accent cis\accent e\accent a\accent cis\accent e\accent
 cis4\accent r r2
 R1*3
 \mark \default
 r4 <c, e> r <bes d>
 r <c e> r <b! dis>
 r <c e> r <bes d!>
 r <c e> r <c e>
 r <c e> r <bes d>
 r <c e> r <b! dis>
 r <c e> r <bes d!>
 <c e>4. <c e>8~ <c e> <e c'>\accent <e c'>\accent <e c'>\accent
 <f d'>4\accent d( c d
 es8\accent) d4( es8 d4 c)
 r d( c d
 es8\accent) d4( es8 d4 c)
 r8 c'\ff c c c(\accent bes) bes bes
 bes(\accent a) a a~\accent a a a a
 a(\accent g) g g~\accent g g g g
 \mark \default
 <b,? g'>\accent r r4 r2
  R1
  <g d' b' g'>8^\markup{"pizz."} r r4 r2
  R1*3
  r2 r4 <a fis'>8\accent\ff^\markup{"arco"} r8
  r <a fis'>8[\accent r <a fis'>8]\accent <a fis'>8\accent r r4\fermata
  \bar "||"
  \mark \default
  \key g \major
  \time 2/2
  <b g'>4\accent\f^\markup{\italic "Con moto"}_\markup{\italic "marcato"} <b g'>4\accent <b g'>4\accent <b g'>4\accent
  <b gis'>\accent <b gis'>\accent <b gis'>\accent <b gis'>\accent
  <c a'> <c a'> <c a'> <c a'>
  \tuplet 3/2 {<es c'>4_\markup{\italic "broadly"} <es bes'> <es c'>} \tuplet 3/2 {<d bes'> <d a'> <d bes'>}%triolenbalken stimmen nicht
  <b! g'>\accent^\markup{\italic "Con moto"} <b g'>\accent <b g'>\accent <b g'>\accent
  <b gis'>\accent <b gis'>\accent <b gis'>\accent <b gis'>\accent
  <c a'> <c a'> <c a'> <c a'>
  <c a'>8 r r4 r2 
  \mark \default
  <e c'>4 <e c'> <e c'> <e c'>
  <es c'> <es c'> <es c'> <es c'>
  <d b'> <d b'> <d b'> <d b'>
  <d b'> <d b'> <d b'> <d b'> 
  <e c'>4 <e c'> <e c'> <e c'>
  <es c'> <es c'> <es c'> <es c'>
  <d b'>\mf <d b'> <d b'> <d b'>
  \override BreathingSign.text = \markup {
    \musicglyph #"scripts.caesura.straight"
  } \breathe
  R1 
  r8^\markup{\italic "a tempo"} <b g'>[_\markup{\italic "poco a poco cresc."} r <b g'> r <b g'> r <b g'>]
  r <b gis'>[ r <b gis'> r <b gis'> r <b gis'>]
  r <d bes'>[ r <d bes'> r <d bes'> r <d bes'>]
  r <d b'!>[ r <d b'> r <d b'> r <d b'>]
  \bar "||"
  \mark \default
  \key c \major
  \repeat percent 2 {r\ff^\markup{"Animato"} <aes f'>[\accent r <aes f'>\accent r <aes f'>\accent r <aes f'>]\accent}
  r <aes ges'>[\accent\> r <aes ges'>\accent r <aes ges'>\accent r <aes ges'>]\accent\pp
  r <aes ges'>[\accent\< r <aes ges'>\accent r <aes ges'>\accent r <aes ges'>]\accent\ff
  \repeat percent 2 { r <aes f'>[\accent r <aes f'>\accent r <aes f'>\accent r <aes f'>]\accent}
  \repeat percent 2 { r <aes ges'>[\accent\> r <aes ges'>\accent r <aes ges'>\accent r <aes ges'>]\accent\pp}
  \mark \default 
  des\mf des des des~\accent\cresc des\!  des des des 
  d!\< d d d es es es es\!
  <gis, e'!> <gis e'> <gis e'> <gis e'> <gis e'> <gis e'!> <gis e'> <gis e'> 
  <gis f'> <gis f'> <gis f'> <gis f'> <gis fis'> <gis fis'> <gis fis'> <gis fis'>
  \repeat unfold 16 {<b g'>}
  
  \mark \default 
  R1*9^\markup{"PIano Solo"}
 % \mark "C" %problem gleichzeitige marks
   \bar "||"
  \mark \default
  R1*19^\markup{ \italic "Meno mosso e poco scherzando"}%Text nicht zentriert?
  
  \mark \default
  \bar "||"
  \key a \major
  R1*8
  R1*4^\markup{"Piano Solo"}
  R1*3
  \mark \default
  R1*12^\markup{"Piano Solo"}
  R1
  \bar "||"
  \key c \major
  \mark \default
  c8\staccato\mp^\markup{"pizz."}^\markup{\italic "Piu mosso"} r8 r4 g8\staccato r r4
  c8\staccato r r4 f8\staccato r r4
  c8\staccato r r4 g8\staccato r r4
  c8\staccato r r4 g8\staccato r r4
  c8\staccato r r4 g8\staccato r r4
  c8\staccato r r4 f8\staccato r r4
  c8\staccato r r4 g8\staccato r r4
  c8\staccato r r4 g8\staccato r r4
  \mark \default
  \repeat percent 4 {aes8\staccato r r4 r2}
  es'8\staccato r r4 bes8\staccato r r4
  es8\staccato r r4 es8\staccato r r4
  es8\staccato r r4 r2
  \mark \default
  R1*13\fermataMarkup_\markup{"Piano Solo"}%Fermate erst am Ende der Pausentakte, nicht mittendrauf
    \override BreathingSign.text = \markup {
    \musicglyph #"scripts.caesura.straight"
  } \breathe
 \bar "||"
 \mark "D"
 R1*4\fermataMarkup%Fermate am Ende
 \mark \default 
 R1*16\fermataMarkup%Fermate auf Taktstrich
 \bar "||"
 \mark \default
 %\mark "E"
 R1*21
 \mark \default
 R1\fermataMarkup\breathe%fermate auf Zäsur fehlt
 \bar "||"
 R1\fermataMarkup%fermate am ende
 \mark "F"
 R1*3
 R1\fermataMarkup
 \bar "||"
 \mark \default
 \key e \major 
 \numericTimeSignature
 \time 4/4
 e4(\p^\markup{\huge "Moderato con espr."} fis gis) gis,~ 
 gis gis( b b)
 ais1~\< 
 ais\!
 a~ 
 a 
 gis~ 
 gis\>
 e'4(\! fis gis) gis,~\( 
 gis gis( b b)\)
 cis1~\< 
 cis\!
 cis~
 cis
 a'4\mf( b cis) \tuplet 3/2 {cis,8( dis cis}
 c4) e(-- e-- e--)
 gis( a b) \tuplet 3/2 {ais,8( cis c)}
 b4 b(\> e e)\!
 \mark \default
 e1~_\markup{\italic "rit."}
 e2. e4 
 es1~ 
 es2\< es\!
 \mark \default
 <gis,! e'!>4\ff^\markup{\italic "a tempo"}^\markup{\italic "Grandioso ma non troppo"} <a fis'> <b gis'> <gis e'>~
 <gis e'> <gis e'> <b gis'> <b gis'>
 <ais e'>\<^\markup{\italic "poco rubato"} <ais e'> <ais e'> <ais e'>
 <ais e'>\> <ais e'> <ais e'> <ais e'>
 <a! dis>\< <a dis> <a dis> <a dis>
 <a! dis>\> <a dis> <a dis> <a dis>
<gis e'>\< <gis e'> <gis e'> <gis e'>
<gis e'>\> <gis e'> <gis e'> <gis e'>\!
<gis e'>\ff^\markup{\italic "Grandioso"} <a fis'> <b gis'> <gis e'>~
<gis e'> <gis e'> <b gis'> <b e>
<a a'>\cresc <a a'>\! <a a'> <a a'>
<a a'> <a a'> <a a'> <a a'>
\repeat unfold 8 {<b gis'>}
<cis a'>\ff <dis b'>_\markup{\italic "allargando"} <e cis'> \tuplet 3/2 {cis8( dis cis)}
c4 \repeat unfold 3 {<c e>}
<b gis'> <cis gis'> <dis gis> \tuplet 3/2 {ais8( cis c)}
b4 <b e> <b fis'> <b e>
\mark \default
gis'\p^\markup{"pizz."} ais c c,~
c d e fis?
gis a b b,~ 
b cis dis2
\mark \default 
R1*36\fermataMarkup^\markup{"Piano Solo"}%Fermate am Ende
\bar "||"
\mark \default
\key a \major
\time 2/4

 
}


\score {
  \new Staff  {\violineTwo}
  \layout {}
}